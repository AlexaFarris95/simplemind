
1. Update `cxr_trachea_template.yml`

2. Run `setup.sh` to generate example_ga_experiment directory

```
cd examples/ga/cxr_trachea_ga_train 
sh setup.sh
```

3. Run `run_ga.sh` inside of the example_ga_experiment directory, to initiate training


```
conda activate deep_med
cd examples/ga/cxr_trachea_train/example_ga_experiment/ga_cxr
sh run_ga.sh
```
