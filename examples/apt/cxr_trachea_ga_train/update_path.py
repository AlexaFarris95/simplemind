"""
Updates the paths in cxr_template for:

    model: /radraid/apps/all/seg_model/cxr_trachea_model.1/cxr_trachea_model
    exp_base: /radraid/apps/personal/wasil/sandbox/ga_experiments
    conf: /cvib2/apps/personal/wasil/lib/qia/qia_latest/ga/conf/cxr/default_conf.yml
    code_base: /cvib2/apps/personal/wasil/lib/qia/qia_latest
    ga_train_list: /scratch2/personal/mdaly/CXR/micro_linux_debug.csv

    experiment:
        trachea_cnn_KerasModel: /scratch/mbrown/CXR/working/trachea_cnn_KerasModel/train_list.csv

    ga_resource:
    - /scratch/mbrown/CXR/working_full_trachea/gpu_user_resource_dir/trachea_cnn_resource.ini

    # MIU standalone
    miu_resource:
    - /scratch/mbrown/CXR/working_full_trachea/gpu_user_resource_dir/trachea_cnn_resource.ini

Updates the paths for image files and references:

Update info yaml files
--> /cvib2/apps/personal/wasil/lib/simplemind/sm_dev/examples/ga/cxr_trachea_ga_train/data/reference/1.2.392.200036.9125.3.19286399523483.64879249256.452495/info.yaml
"""

import os, pandas as pd, yaml, glob

base_dir = os.path.dirname(os.path.abspath(__file__))


relevant_paths = dict(  model= os.path.join(base_dir, "cxr_trachea_model.1", "cxr_trachea_model"), 
                        exp_base= os.path.join(base_dir, "example_ga_experiment"),
                        optimizer_conf = os.path.join(base_dir, "ga_optimizer_conf.yml"),
                        distributor_conf = os.path.join(base_dir, "local_distributor_conf.yml"),
                        condor_distributor_conf = os.path.join(base_dir, "condor_distributor_conf.yml"),
                        task_conf = os.path.join(base_dir, "task.yml"),
                        code_base= os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(base_dir)))),
                        report=os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(base_dir))), "simplemind", "apt_agents", "optimizer", "ga", "config", "cxr", "trachea_template.tpl"),
                        subreport=os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(base_dir))), "simplemind", "apt_agents", "optimizer", "ga", "config", "cxr", "trachea_sub_template.tpl"),
                        apt_train_list = os.path.join(base_dir, "apt_train_list.csv"),
                        # experiment = dict(trachea_cnn_KerasModel={"train_list.csv": os.path.join(base_dir, "trachea_cnn_KerasModel","train_list.csv")}),
                        experiment = dict(trachea_cnn_KerasModel={"train_list.csv": os.path.join(base_dir, "trachea_cnn_train_list.csv")}),
                        distributor_resource = [os.path.join(base_dir, "trachea_cnn_resource.ini"), ],
                        local_resource = [os.path.join(base_dir, "trachea_cnn_resource.ini"), ],
)

### can potentially activate later ###
# ga_train_df = pd.read_csv(relevant_paths["ga_train_list"])
# ga_train_df["reference"] = os.path.join(os.path.join(base_dir, "data", "reference"))
# for i, row in ga_train_df.iterrows():
#     ### make .seri file
#     seri_file = os.path.join(os.path.join(base_dir, "data", "image", ga_train_df["id"][i])+"_0.seri")
    
#     # naive way of creating .seri files -- preferably do by opening each dicom file and seeing the physical z location
#     # dcm_files = os.listdir(os.path.join(os.path.join(base_dir, "data", "image", ga_train_df["id"][i]+"_0")))
#     dcm_files = glob.glob(os.path.join(os.path.join(base_dir, "data", "image", ga_train_df["id"][i]+"_0", "*.dcm")))
#     dcm_files = sorted(dcm_files, key=lambda t:int(os.path.splitext(os.path.basename(t))[0]))
#     contents = "\n".join(dcm_files)  
#     with open(seri_file, 'w') as f:
#         f.write(contents)

#     ### update image_path
#     ga_train_df["image_file"][i] = seri_file

#     ### update ref path
#     ref_info_file = os.path.join(ga_train_df["reference"][i], ga_train_df["id"][i], "info.yaml")
#     with open(ref_info_file, 'r') as f:
#         contents = yaml.load(f, Loader=yaml.FullLoader)
#     if contents.get("TraCh", dict()).get("roi_path"):
#         contents["TraCh"]["roi_path"] = os.path.join(ga_train_df["reference"][i], ga_train_df["id"][i], "TraCh.roi")
#     if contents.get("NgTub", dict()).get("roi_path"):
#         contents["NgTub"]["roi_path"] = os.path.join(ga_train_df["reference"][i], ga_train_df["id"][i], "NgTub.roi")
#     with open(ref_info_file, 'w') as f:
#         f.write(yaml.dump(contents))

# ga_train_df.to_csv(relevant_paths["ga_train_list"])

    
with open("cxr_trachea_template.yml", 'r') as f:
    contents = yaml.load(f, Loader=yaml.FullLoader)

contents.update(relevant_paths)

with open("cxr_trachea_template_initialized.yml", 'w') as f:
    f.write(yaml.dump(contents))