### Run this bash file inside the following docker container:
#
# docker run --rm -it -u $(id -u):$(id -g) -v $PWD:/workdir -v /cvib2:/cvib2 -v /cvibraid:/cvibraid -v /scratch:/scratch -v /radraid:/radraid -v /scratch2:/scratch2 -v ~/.local:/.local -w /workdir sm_release:latest bash
#


export entry_point=%s
export version=%s
export base_dir=%s/$version
export case_csv=$base_dir/experiment/apt_train_list.csv
export model_file=$base_dir/sm_model/%s
export results_path=$base_dir/result
export distributor_config=$base_dir/distributor.yml
export task_config=$base_dir/task.yml
export log=$base_dir/assess.log

python $entry_point $case_csv $model_file $results_path --distributor_config $distributor_config --task_config $task_config | tee $log