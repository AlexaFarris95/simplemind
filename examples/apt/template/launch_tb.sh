export tb_port=%s
export logdir=experiment
export cmd="tensorboard --logdir=${logdir} --port=${tb_port}"
docker run -it -p $tb_port:$tb_port -u $(id -u):$(id -g) -v $PWD:/workdir -v ~/.local:/.local -v /radraid:/radraid -w /workdir registry.cvib.ucla.edu/medqia:prod bash -c "$cmd"