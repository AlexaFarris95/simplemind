# source /scratch/wasil/env/etc/profile.d/conda.sh
# conda activate deep_med
#### uses yaml, deap that's only in deep_med

export entry_point=%s
export version=%s
export base_dir=%s/$version
export checkpoint=$base_dir/optimizer_checkpoint/pilot.pk
export case_csv=$base_dir/experiment/apt_train_list.csv
export model_file=$base_dir/sm_model/%s
export results_path=$base_dir/result
export optimizer_config=$base_dir/optimizer.yml
export distributor_config=$base_dir/distributor_condor.yml
export task_config=$base_dir/task.yml
export log=$base_dir/optimize_condor.log


python $entry_point $case_csv $model_file $results_path --checkpoint $checkpoint --optimizer_config $optimizer_config --distributor_config $distributor_config --task_config $task_config | tee $log