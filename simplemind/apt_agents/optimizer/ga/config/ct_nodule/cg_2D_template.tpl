<!DOCTYPE html>
<html>

<head>
<style>
table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
	padding: 10px;

}
}

.equ {

	border: 0px;
	padding: 10px;
	vertical-align: top;
}
th {
  text-align: left;
}

table#t01 {
	width:100%;
}

table#t01 tr:nth-child(even) {
  background-color: #eee;
}
table#t01 tr:nth-child(odd) {
 background-color: #fff;
}

table#t01 th {
 background-color: #fff;
}


table#t02 tr:nth-child(even) {
  background-color: #fff;
}
table#t02 tr:nth-child(odd) {
 background-color: #fff;
}

</style>
</head>

<body>

<h1>
{{ gene }} <br>
{{ gene_binary }}
</h1>

<h3>
Fitness: {{ final_result["fitness"] }}
</h3>

<!-- <img width=100% src=" to_html(final_result["error_hist_plot"]) " > -->


<h1>
Overall Performance Summary
</h1>

<table class="equ" id="t01">
<!-- 	<col width="200">
	<col width="175">
	<col width="175">
	<col width="175">
	<col width="175"> -->
	<tr class="equ">
		<th class="equ" width=120px> </th>
		<th class="equ">Nodule Candidate Generation</th>
		<!-- <th class="equ">GE Junction</th>
		<th class="equ">ETT Tip</th>
		<th class="equ">NG Tube</th> -->
	</tr>

	<!-- <tr class="equ">
		<th class="equ">[DCE Score]</th>
		<td class="equ"></td>
	</tr>
	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dce_score_mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dce_score_std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dce_score_median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dce_score_q1"] }}, {{ final_result["slice_nodule_ref"]["dce_score_q3"] }}</td>
	</tr> -->
	<tr class="equ">
		<th class="equ">[Dice Coefficient]</th>
		<td class="equ"></td>
	</tr>
	<tr class="equ">
		<th class="equ">mean</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dice_coefficient_mean"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">std</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dice_coefficient_std"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">median</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["dice_coefficient_median"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">1st/3rd quartiles</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["sensitivity_q1"] }}, {{ final_result["slice_nodule_ref"]["sensitivity_q3"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number annotated cases:</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["num_annotated"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Number good predictions:</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["num_good_markings"] }} cases with meeting {{ final_result["slice_nodule_ref"]["threshold"] }} threshold</td>
	</tr>
	<!-- <tr class="equ">
		<td class="equ">Bad markings:</td>
		<td class="equ">{{ final_result["slice_nodule_ref"]["num_bad_markings"] }}</td>
	</tr> -->
	<tr class="equ">
		<th class="equ">Detection:</th>
		<td class="equ">
			<table class="list" id="t02">
				<tr class="list">
					<td class="list">TP: {{ final_result["slice_nodule_ref"]["tp"] }}</td>
					<td class="list">FP: {{ final_result["slice_nodule_ref"]["fp"] }}</td>
				</tr>
				<tr class="list">
					<td class="list">FN: {{ final_result["slice_nodule_ref"]["fn"] }}</td>
					<td class="list">TN: {{ final_result["slice_nodule_ref"]["tn"] }}</td>
				</tr>
			</table>

		</td>
	</tr>

	<tr class="equ">
		<th class="equ">Sensitivity:</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["sensitivity"] }}</td>
	</tr>
	<tr class="equ">
		<th class="equ">Specificity:</th>
		<td class="equ">{{ final_result["slice_nodule_ref"]["specificity"] }}</td>
	</tr>

</table>

<!-- 
# - ref present/absent
# - MIU output present/absent
# - TP/FP/FN/TN
# - DCE Score
# - DCE -->


{% if final_result["bad_cases"]["slice_nodule_ref"] -%}
<h1>High error cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Sensitivity</th>
		<th class="list">FP</th>
		<th class="list">FN</th>
	</tr>
{% for key in final_result["bad_cases"]["slice_nodule_ref"] -%}
	<tr class="list">
		<td class="list"><a href="sub_report/{{key}}.html">{{key}}</a></td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("sensitivity")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fp")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fn")}}</td>
	</tr>
{%- endfor %}
</table>
{%- endif %}


{% if final_result["good_cases"]["slice_nodule_ref"] -%}
<h1>Other cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Sensitivity</th>
		<th class="list">FP</th>
		<th class="list">FN</th>
	</tr>
{% for key in final_result["good_cases"]["slice_nodule_ref"] -%}
	<tr class="list">
		<td class="list"><a href="sub_report/{{key}}.html">{{key}}</a></td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("sensitivity")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fp")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fn")}}</td>
	</tr>
{%- endfor %}
</table>


{% else -%}
<h1>All cases</h1>
<table class="list">
	<tr class="list">
		<th class="list">Case</th>
		<th class="list">Sensitivity</th>
		<th class="list">FP</th>
		<th class="list">FN</th>
	</tr>
{% for key in keys -%}
	<tr class="list">
		<td class="list"><a href="sub_report/{{key}}.html">{{key}}</a></td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("sensitivity")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fp")}}</td>
		<td class="list">{{final_result["per_case_result"][key]["slice_nodule_ref"].get("fn")}}</td>
	</tr>
{%- endfor %}
</table>
{%- endif %}


</body>

</html>