<!DOCTYPE html>
<html>

<head>
<style>
table.list {
	border-collapse: collapse;
}
.list {
    border: 1px solid black;
}

.equ {
	vertical-align: top;
	border: 0px;
}

.swap img {
	width: auto;
	height: auto;
}
.swap img.org{display:none}
.swap:hover img.overlay{display:none}
.swap:hover img.org{display:inline-block}
</style>
</head>

<body>

<h1>
{{ key }}
</h1>

<h2>Overall</h2>
<table class="equ">
	<tr class="equ">
		<td class="equ"></td>
		<td class="equ">All</td>
		<td class="equ">CNN-only</td>

	</tr>
	<tr class="equ">
		<td class="equ">TP:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"]["tp"] }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"]["tp"] }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">FP:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"]["fp"] }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"]["fp"] }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">FN:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"]["fn"] }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"]["fn"] }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">Sensitivity:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"]["sensitivity"] }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"]["sensitivity"] }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">Normalized FP:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"].get("normalized_fp", "None") }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"].get("normalized_fp", "None") }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">Overlap ratio:</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref"].get("overlap_ratio", "None") }}</td>
		<td class="equ">{{ per_case_result["slice_nodule_ref_2"].get("overlap_ratio", "None") }}</td>
	</tr>
	<tr class="equ">
		<td class="equ">Slice spacing (in z-axis):</td>
		<td class="equ">{{ per_case_result["spacing"][2] }}</td>
	</tr>
</table>

<h2>Missed (FN)</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">Reference ID</th>
		<th class="list">Diameter</th>
		<th class="list">Perpendicular Diameter</th>
	</tr>
	
{% for nodule in per_case_result["slice_nodule_ref"]["detections"] -%}
	{{ nodule["type"] }}

	{% if nodule["type"]=="FN" -%}

	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("truth_id", "--")}}</td>
		<td class="list">{{nodule.get("truth_diameter", "--")}}</td>
		<td class="list">{{nodule.get("truth_perp_diameter", "--")}}</td>		
	</tr>
	{%- endif %}
{%- endfor %}
</table>

<h2>True Positives</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">Reference ID</th>
		<th class="list">Diameter</th>
		<th class="list">Perpendicular Diameter</th>
		<th class="list">CAD ID</th>
		<th class="list">CAD Diameter</th>
		<th class="list">CAD Perpendicular Diameter</th>
	</tr>
{% for nodule in per_case_result["slice_nodule_ref"]["detections"] -%}
	{% if nodule["type"]=="TP" -%}
	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("truth_id", "--")}}</td>
		<td class="list">{{nodule.get("truth_diameter", "--")}}</td>
		<td class="list">{{nodule.get("truth_perp_diameter", "--")}}</td>
		<td class="list">{{nodule.get("id", "--")}}</td>
		<td class="list">{{nodule.get("diameter", "--")}}</td>
		<td class="list">{{nodule.get("perp_diameter", "--")}}</td>			
	</tr>
	{%- endif %}
{%- endfor %}
</table>

<h2>False Positives</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">CAD ID</th>
		<th class="list">CAD Diameter</th>
		<th class="list">CAD Perpendicular Diameter</th>
	</tr>
{% for nodule in per_case_result["slice_nodule_ref"]["detections"] -%}
	{% if nodule["type"]=="FP" -%}
	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("id", "--")}}</td>
		<td class="list">{{nodule.get("diameter", "--")}}</td>
		<td class="list">{{nodule.get("perp_diameter", "--")}}</td>			
	</tr>
	{%- endif %}
{%- endfor %}
</table>


<h1> CNN-only </h1>
<h2>Missed (FN)</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">Reference ID</th>
		<th class="list">Diameter</th>
		<th class="list">Perpendicular Diameter</th>
	</tr>
	
{% for nodule in per_case_result["slice_nodule_ref_2"]["detections"] -%}
	{{ nodule["type"] }}

	{% if nodule["type"]=="FN" -%}

	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("truth_id", "--")}}</td>
		<td class="list">{{nodule.get("truth_diameter", "--")}}</td>
		<td class="list">{{nodule.get("truth_perp_diameter", "--")}}</td>		
	</tr>
	{%- endif %}
{%- endfor %}
</table>

<h2>True Positives</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">Reference ID</th>
		<th class="list">Diameter</th>
		<th class="list">Perpendicular Diameter</th>
		<th class="list">CAD ID</th>
		<th class="list">CAD Diameter</th>
		<th class="list">CAD Perpendicular Diameter</th>
	</tr>
{% for nodule in per_case_result["slice_nodule_ref_2"]["detections"] -%}
	{% if nodule["type"]=="TP" -%}
	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("truth_id", "--")}}</td>
		<td class="list">{{nodule.get("truth_diameter", "--")}}</td>
		<td class="list">{{nodule.get("truth_perp_diameter", "--")}}</td>
		<td class="list">{{nodule.get("id", "--")}}</td>
		<td class="list">{{nodule.get("diameter", "--")}}</td>
		<td class="list">{{nodule.get("perp_diameter", "--")}}</td>			
	</tr>
	{%- endif %}
{%- endfor %}
</table>

<h2>False Positives</h2>
<table class="list">
	<tr class="list">
		<th class="list">Axial,Coronal,Sagittal</th>
		<th class="list">CAD ID</th>
		<th class="list">CAD Diameter</th>
		<th class="list">CAD Perpendicular Diameter</th>
	</tr>
{% for nodule in per_case_result["slice_nodule_ref_2"]["detections"] -%}
	{% if nodule["type"]=="FP" -%}
	<tr class="list">
		<td class="list">
			<a class="swap">
			<img class="overlay" src="{{ to_html(nodule["a_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["a_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["c_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["c_org"]) }}"/>
			
			<img class="overlay" src="{{ to_html(nodule["s_ovr"]) }}"/>
			<img class="org" src="{{ to_html(nodule["s_org"]) }}"/>
			</a>
		</td>
		<td class="list">{{nodule.get("id", "--")}}</td>
		<td class="list">{{nodule.get("diameter", "--")}}</td>
		<td class="list">{{nodule.get("perp_diameter", "--")}}</td>			
	</tr>
	{%- endif %}
{%- endfor %}
</table>



</body>

</html>