from simplemind.apt_agents.optimizer.ga.src.core.evaluate import Evaluator, ResultsCompiler, read_miu_results
import os, yaml, glob, numpy as np
import logging

log = logging.getLogger("nodule_cg")

try:
    from simplemind import __qia__
    import qia.common.img.image as qimage
    import qia.common.img.overlay as qovr
    from qia.common.img.utils import get_casted_roi
    from qia.common.img.region import Region
except:
    log.warning("Can't import QIA img modules.")
# from qia.segmentation.ct_nodule import get_template_from_outpath, iter_outpath_nodule2

# from qia.segmentation.ct_nodule.conf import DEFAULT_TEMPLATE, DEFAULT_SUB_TEMPLATE
# from qia.segmentation.ct_nodule.reference import load as load_reference
# from qia.segmentation.ct_nodule.report import generate_report

SCREENSHOT_SIZE_MM = 50
LEVEL = -550
WINDOW = 1600

def get_default_orientation():
    ORIENTATION = {
        "c": dict(crsstype=qovr.CrossSection.custom, xaxis=(1,0,0), yaxis=(0,0,-1)),
        "s": dict(crsstype=qovr.CrossSection.custom, xaxis=(0,1,0), yaxis=(0,0,-1)),
        "a": dict(crsstype=qovr.CrossSection.axial),
    }
    return ORIENTATION


"""
Generating a screenshot per nodule
    :param pos: center position of nodule (image coordinates)
    :param image: loaded QIA image obj 
    :param mask: loaded detected nodule ROI
    :param prefix: path/naming prefix  
    :param truth_mask: [optional] loaded matching truth ROI
    :param paths_only: True -> Not making the images, just returning the paths 

- Will save to "[prefix]_[k]_org.png" where [prefix] is the prefix path (eg "/path/to/something/with_this_prefix")
    and [k] is the orientation -- in this case, "c", "s", or "a" (coronal, sagittal, axial)
- Will make an original "org" version without overlay, and overlay version (without "org")
"""
def generate_screenshot(pos, image, mask, prefix, truth_mask=None, paths_only=False, region=None):
    ret = {}

    if not paths_only and region is None:
        minp = image.to_image_coordinates([i-SCREENSHOT_SIZE_MM/2 for i in pos])
        maxp = image.to_image_coordinates([i+SCREENSHOT_SIZE_MM/2 for i in pos])
        region = [[round(min(i,j)) for i,j in zip(minp, maxp)], [round(max(i,j)) for i,j in zip(minp, maxp)]]
    ret = {}
    ORIENTATION = get_default_orientation()
    for k,o in ORIENTATION.items():
        log.debug("{} {}".format(k,o))
        org_file = "%s_%s_org.png" % (prefix, k)
        ovr_file = "%s_%s.png" % (prefix, k)
        if not paths_only:  # arranges and writes screenshots
            ### This is just a fancy way to handle all the screenshot orientations in one object
            gen = qovr.auto(pos, image=image, region=region, **o)   
            gen.set(image, LEVEL-WINDOW/2, LEVEL+WINDOW/2, boundval=-1000)
            gen.write(org_file)
            if mask is not None:
                gen.add(mask, (255,0,0), .5, boundval=0)
            if truth_mask is not None:
                gen.add(truth_mask, (0,255,0), 0.2, boundval=0)
            gen.write(ovr_file)
        ### filling dict with file paths
        ret["%s_org" % k] = org_file
        ret["%s_ovr" % k] = ovr_file
    return ret


class Evaluator_Nodule_CG(Evaluator):
    def __init__(self):
        visuals = ["nodule_cnn_*.png",]
        roi_dict = {"nodule": "slice_nodule_ref"} ### TODO: is this 2D ???? or 3D??? does this need to be changed?
        rois = list(roi_dict.keys())
        super().__init__(rois=rois, visuals=visuals, roi_dict=roi_dict)
        return


    def load_reference(self, reference_path, image=None, dataset=None, id=None):
        """Loads the reference data for a single CT slice.
        Assumes all references are held case-wise in "[reference_path]\[uid]\info.yaml"

        Each info.yaml will have two paths -- one to the 3D reference roi, and one to the 3D image paired with the 3D reference roi
        """
        with open(os.path.join(reference_path, 'info.yml')) as f:
            reference_contents = yaml.load(f)
        reference_contents["slice_nodule_ref"]["roi"] = None
        try:
            import qia.common.img.image as qimage
        except:
            log.warning("Failed to import qimage (evaluate_nodule_cg.py)")
            
        ## in every 2D slice dictionary, the reference is a 3D image held in ["slice_nodule_ref"]["image_path"]
        if image is None:
            log.debug("slice_nodule_ref upon loading: {}".format(reference_contents))
            if reference_contents["slice_nodule_ref"].get("image_path") is not None:
                image = qimage.read(reference_contents["slice_nodule_ref"]["image_path"])
                reference_contents["slice_nodule_ref"]["image"] = image
            else:
                raise("I didn't do this right :(")
        log.debug("slice_nodule_ref %s", reference_contents["slice_nodule_ref"])

        ### reference ROI is in 3D, so it needs a 3D version of all the images
        if reference_contents["slice_nodule_ref"].get("roi_path") is not None and image is not None:

            roi = qimage.cast(image)
            roi.fill_with_roi(reference_contents["slice_nodule_ref"]["roi_path"])
            # roi = qimage.read(reference_contents["slice_nodule_ref"]["roi_path"])

            reference_contents["slice_nodule_ref"]["roi"] = roi

            ### need to separate ROI into multiple components
            roi_np = reference_contents["slice_nodule_ref"]["roi"].get_array()
            connectivity = 18 # only 4,8 (2D) and 26, 18, and 6 (3D) are allowed
            import cc3d
            roi_cca_np = cc3d.connected_components(roi_np, connectivity=connectivity)
            np.save("/cvib2/apps/personal/wasil/trash/truth_debug.npy", roi_cca_np)
            reference_contents["slice_nodule_ref"]["indexed_roi"] = qimage.from_array(roi_cca_np, template=reference_contents["slice_nodule_ref"]["roi"])  # replace binary ROI qia object with indexed ROI qia object

            reference_contents["slice_nodule_ref"]["separated_rois"] = {}
            ### compute metrics for each component
            for index, separated_roi in self._iter_detections(reference_contents["slice_nodule_ref"]["indexed_roi"]):
                truth_id = "ref_{}".format(index)
                reference_contents["slice_nodule_ref"]["separated_rois"][truth_id] = self._compute_roi_measures(separated_roi)
                reference_contents["slice_nodule_ref"]["separated_rois"][truth_id]["region"] = separated_roi.get_region()
                reference_contents["slice_nodule_ref"]["separated_rois"][truth_id]["mask"] = separated_roi
              


        return reference_contents

    def roi_wise_visualization(self, res, template, outpath, roi_out, skip_images=False, extra=None):
        if extra is None: extra = dict()
        import qia.common.img.overlay as qovr
        pos = list(template.to_image_coordinates(qovr.get_centroid(roi_out)))
        pos[2] = res["z"]
        pos = list(template.to_physical_coordinates(pos))
        res_ss = dict()
        if res.get("type")=="TP":
            res_ss.update(generate_screenshot(pos, template, roi_out, os.path.join(outpath, "%s_TP" % res["id"]), truth_mask=res["truth_mask"]))
        elif res.get("type")=="pTP":
            if res.get("pTP_reason", "overlap")=="fp_ref":
                    # specific for nodule detection
                res_ss.update(generate_screenshot(pos, template, roi_out, os.path.join(outpath, "%s_pTP" % res["id"]), truth_mask=extra.get("fp_ref")))
            else:   # default pTP screenshot
                res_ss.update(generate_screenshot(pos, template, roi_out, os.path.join(outpath, "%s_pTP" % res["id"]), truth_mask=res["truth_mask"]))
        elif res.get("type")=="FP":
            res_ss.update(generate_screenshot(pos, template, roi_out, os.path.join(outpath, "%s_FP" % res["id"]), paths_only=skip_images))
        else:
            log.debug("No classification given for this ROI.")
        return res_ss

    ### this version allows classification as pseudo true positive, if "fp_ref" is provided
    def _process_match(self, res, roi_out, extra=None):
        if extra is None: extra=dict()
        # res = {}
        if res.get("truth_mask") is not None:  # if there is a match with "hard" truth, a truth_mask key should be in the dictionary                  
            if res["dice_coefficient"] > .25:    #041217 added Dice Coefficient requirement for true positive
                res["type"] = "TP"
                # detected_truths.add(matched_truth_info["index"])
            else:
                res["type"] = "pTP"
                res["pTP_reason"] = "overlap"
        elif extra.get("fp_ref") is not None: ### LIDC specific
            # compute intersection between soft positives truth ROI and the nodule 
            joint_hist = roi_out.get_joint_histogram(extra["fp_ref"])
            if joint_hist.get((1,1), 0)>0:  # if there is a match, then pTP
                res["type"] = "pTP"
                res["pTP_reason"] = "fp_ref"
            else:                           # elsewise, FP
                res["type"] = "FP"
        else:
            res["type"] = "FP"
        return res

    # ### MAIN FUNCTION ###
    # def evaluate_task(self, result_path, reference_path, outpath, id=None, dataset=None, skip_images=False):
    #     log.debug("Line 212: Entering default EVALUATE 'evaluate_task' ")
    #     """Evaluates performance for a single case.

    #     Loads predictions from saved .rois and loads relevant reference. These are
    #     passed onto an evaluation computation function. Essentially a wrapper to
    #     load things differently if there is a different dataset/ output/ reference
    #     format.

    #     Process:
    #     (1) Load MIU output
    #     (2) Load CXR Reference
    #     (3) Compute evaluation metric

    #     :param result_path: A directory containing the segmentation results.
    #     :param reference_path: A directory containing the reference, expects a
    #                         file named "info.yaml" in it.
    #     :param outpath: A directory containing the evaluation results.
    #     """
    #     # read in relevant ROIs
    #     image, rois, extra = self.read_miu_results(result_path, rois=self.rois, visuals=self.visuals ) 
    #     #### return lists of 2D detected rois

    #     # load reference from the case's directory
    #     reference = self.load_reference(reference_path, image=image, dataset=dataset, id=id)

    #     # CUSTOMIZED -- assign the loaded 3D reference image into `image` variable
    #     # image = reference["slice_nodule_ref"]["image"]
    #     return self._compute_evaluation_measures(image, rois, reference, outpath, skip_images=skip_images, extra=extra)


    def casewise_visualization(self, outpath, image, case_dict, rois, extra):
        return None
        image_dir = os.path.join(outpath, "image")
        os.makedirs(image_dir, exist_ok=True)
        # image_arr = image.get_array()[0]
        # image_arr = img_as_float64(image_arr)

        image_path = os.path.join(image_dir, "original.png")
        marking_path = os.path.join(image_dir, "markings.png")
        vis_dict = dict()

        ### estimate central position to be:
        phys_region = image.get_physical_region() # ( (x0,y0,z0), (x1, y1, z1) ) 
        pos = ( (phys_region[0][0]+phys_region[1][0])/2, (phys_region[0][1]+phys_region[1][1])/2, (phys_region[0][2]+phys_region[1][2])/2)
        image_region = image.get_region()
        # if rois["nodule_cnn"] is not None:
        #     log.debug(rois["nodule_cnn"])
        #     output_roi = qimage.cast(image)
        #     output_roi.fill_with_roi(rois["nodule_cnn"])
        # else:
        #     output_roi = None
        ss_dict = generate_screenshot(pos, image, rois["nodule_cnn"], os.path.join(outpath, "nodule_slice" ), truth_mask=case_dict["slice_nodule_ref"]["reference_roi"], region=image_region)
        ### we want "a_org" "a_ovr"
        vis_dict["marking_ss"] = ss_dict["a_ovr"]
        vis_dict["original_ss"] = ss_dict["a_org"]
        
        ### You can also pull anything that is preprocessed in MIU and read in through loading ``visuals`` in __init__
        ### They will be stored in ``extra``
        vis_dict["preprocessed_ss"] = extra.get("visuals", ["path/to/fake/image",])
        if vis_dict["preprocessed_ss"]:
            vis_dict["preprocessed_ss"] = vis_dict["preprocessed_ss"][0]
        else:
            vis_dict["preprocessed_ss"]  = "path/to/fake/image"
        return vis_dict

    def _compute_evaluation_measures(self, image, rois, reference, outpath, skip_images=False, extra=None):
        # return self._compute_evaluation_measures_segmentation(image, rois, reference, outpath, skip_images=skip_images, extra=extra)
        # self._compute_evaluation_measures_detection(template, truth, result_path, outpath, extra=extra, skip_images=skip_images)
        return self._compute_evaluation_measures_detection(image, rois, reference, outpath, skip_images=skip_images, extra=extra)


    def _custom_dce_score(self, reference, rois, ref_key, miu_key):
        # reference[ref_key]["roi"] is from reference
        # rois[miu_key] is from MIU
        log.debug("ROIs {}".format(rois))
        dice_coefficient = None
        dce_score = None
        if reference[ref_key].get("roi") is not None:   # if ref exists
            ref_detection = 1
            if rois.get(miu_key) is None:
                dce_score = 0
                pred_detection = 0
                detection_class = "FN"
            else:
                joint_hist = rois[miu_key].get_joint_histogram(reference[ref_key]["roi"])
                dice_coefficient = 0
                if joint_hist.get((1,1), 0)>0:
                    aNb = joint_hist[(1,1)]
                    aUb = 0
                    for k,v in joint_hist.items():
                        if k!=(0,0):
                            aUb += v
                    #dice coefficient defined by 2*(XnY)/(X + Y) --> 2*(XnY)/(XnY+XuY)
                    dice_coefficient = 2*aNb/(aNb+aUb)
                # dce_score = min(dice_coefficient, 0.95)
                dce_score = dice_coefficient
                pred_detection = 1
                detection_class = "TP"
        else:   # ref is absent
            ref_detection = 0
            if rois.get(miu_key) is None:
                dce_score = 1
                pred_detection = 0
                detection_class = "TN"
            else:
                dce_score = 0 
                pred_detection = 1
                detection_class = "FP"
        return dict(dce_score=dce_score, dice_coefficient=dice_coefficient, pred_detection=pred_detection, 
                        detection_class=detection_class, ref_detection=ref_detection, )

## Include in every evaluation script
## returns case dictionary of results
def evaluate_task(param, previous_output=None):
    result_path, reference_path, outpath, dataset, case_id, skip_images = param

    ## in case this was done sequentially with sm.runner
    if previous_output is not None and previous_output.get("result_path"):
         result_path = previous_output.get("result_path")
    if skip_images is None: skip_images=False

    evaluator  = Evaluator_Nodule_CG()    ## TODO: potentially generalizable if evaluator is passed through param
    case_dict = evaluator.evaluate_task(result_path, reference_path, outpath, dataset=dataset, id=case_id, skip_images=skip_images)
    case_dict["finished"] = True

    return case_dict

def fitness_nodule_cg(final_result):
    return final_result["slice_nodule_ref"]["dce_score_raw"]

### Include in every evaluation task
from simplemind.apt_agents.optimizer.ga.src.conf import CONFIG_PATH

### TODO: Create template file for visiualization/report
DEFAULT_TEMPLATE = os.path.join(CONFIG_PATH, "ct_nodule", "cg_template.tpl")
DEFAULT_SUB_TEMPLATE = os.path.join(CONFIG_PATH, "ct_nodule", "cg_sub_template.tpl")
# DEFAULT_TEMPLATE = os.path.join(CONFIG_PATH, "renal", "renal_template.tpl")
# DEFAULT_SUB_TEMPLATE = os.path.join(CONFIG_PATH, "renal", "renal_sub_template.tpl")

###################
class ResultsCompiler_Nodule_CG(ResultsCompiler):
    def __init__(self, fitness_func=fitness_nodule_cg, overall_report_template=DEFAULT_TEMPLATE, casewise_report_template=DEFAULT_SUB_TEMPLATE ):
        super().__init__(fitness_func=fitness_func, overall_report_template=overall_report_template, casewise_report_template=casewise_report_template)
        log.debug("overal_report_template %s", overall_report_template)
        ### This section for performance stratification, which is optional ###
        self.thresh = {'slice_nodule_ref': 0.80,
                }

        self.bad_cases = dict()
        self.good_cases = dict()
        for k in self.thresh.keys():
            self.bad_cases[k] = []
            self.good_cases[k] = []

        self.ref_keys = ["slice_nodule_ref",] # markings/outputs we're interested in
    
    ### Example of stratification for cxr trachea
    # def stratify_performance(self, labelwise_result, ref_key, ):
    #     print(labelwise_result["sensitivity"], self.thresh[ref_key])
    #     if labelwise_result["sensitivity"] < self.thresh[ref_key]:
    #         return False
    #     return True
    
    def stratified_performer_metrics(self, ref_key):
        strat_perf_dict = {
                            "num_bad_markings": len(self.bad_cases[ref_key]),
                            "num_good_markings": len(self.good_cases[ref_key]),
                            "threshold": self.thresh[ref_key],
        }
        return strat_perf_dict
    
    def compile_function(self, result_dictionary, outpath, rerun=False, canary=False):
        self.compile_function_detection(result_dictionary, outpath,rerun=rerun,canary=canary)
        




## Include in every evaluation script
def compile_function(param, previous_output=None):
    cases, outpath, rerun, report_templates, canary = param
    log.debug("parameters: %s", param)
    if rerun is None: rerun=False
    if canary is None: canary=False
    if report_templates is None: report_templates = dict()

    results_compiler  = ResultsCompiler_Nodule_CG(overall_report_template=report_templates.get("report"), casewise_report_template=report_templates.get("subreport"))

    if previous_output is not None and previous_output.get("result_dictionary") is not None:
        result_dictionary = previous_output["result_dictionary"]
    else:
        result_dictionary = results_compiler.load(cases, results_dir=outpath)

    fitness_tuple = results_compiler.compile_function(result_dictionary, outpath, rerun=rerun, canary=canary)

    return dict(fitness=fitness_tuple, finished=True)
