import os

def _gen_ref(case):
    ref = None
    if case["dataset"]=="lidc":
        ref = os.path.join(case["reference"], case["id"])
        if not os.path.exists(ref):
            raise IOError("Cannot find %s" % ref)
    elif case["dataset"]=="cxr":
        ref = os.path.join(case["reference"], case["id"])
        if not os.path.exists(ref):
            raise IOError("Cannot find %s" % ref)
    else:
        if os.path.exists(case["reference"]):
            ref = case["reference"]
    return ref





class Region():
    def __init__(self, region):
        self.region_min, self.region_max = region 

    def min_max(self):
        return (self.region_min, self.region_max)
    def _in_region(self, pt):   # pt just has to match the dimensions of region_min/region_max
        outside = False
        for i_coord, coord in enumerate(pt):
            if coord < self.region_min[i_coord] or coord > self.region_max[i_coord]:
                outside = True
                break
        return not outside
    
    def get_bounding_points(self):
        points = []
        points.append(self.region_min)
        for min_coord in range(len(self.region_min)): # for the min value of every dimension
            pt = []
            [pt.append(None) for i in range(len(self.region_min))]
            pt[min_coord] = self.region_min[min_coord]
            for max_coord in range(len(self.region_max)): # for the max value of every dimension
                if min_coord==max_coord: continue
                pt[max_coord] = self.region_max[max_coord]
            points.append(pt)

        points.append(self.region_max)
        for max_coord in range(len(self.region_max)):
            pt = []
            [pt.append(None) for i in range(len(self.region_max))]
            pt[max_coord] = self.region_max[max_coord]
            for min_coord in range(len(self.region_min)):
                if min_coord==max_coord: continue
                pt[min_coord] = self.region_min[min_coord]
            points.append(pt)

        # some returned as tuples
        list_pts = []
        for pt in points:
            list_pts.append(list(pt))
        return list_pts
        
    def get_bounding_lines(self):
        
        bounding_pts1 = self.get_bounding_points()
        # print("pts1", bounding_pts1)
        bounding_pts2 = self.get_bounding_points()
        # print("pts2", bounding_pts2)
        line_pts = set() # set with all points on all edge lines of bounding box
        
        dimension = len(bounding_pts1[0])
        for _bounding_pt1 in bounding_pts1:
            for _bounding_pt2 in bounding_pts2: # for every bounding point, compare it to every other bounding point
            # if it is on the same axis (meaning all but one coords (x,y,z) stay constant)
                bounding_pt1 = list(_bounding_pt1)
                bounding_pt2 = list(_bounding_pt2)
                # print("pt1: ", bounding_pt1, " pt2: ", bounding_pt2)
                count = 0
                chn_idx = 0
                for idx in range(dimension):
                    if(bounding_pt1[idx] != bounding_pt2[idx]):
                        chn_idx = idx
                        count += 1
                
                if(count == 1): # if on same axis, generate the points in between the two bounding points
                    # setting which one is max which one is min
                    # print("changing index: ", chn_idx)
                    if (bounding_pt1[chn_idx] > bounding_pt2[chn_idx]):
                        max_coord = bounding_pt1[chn_idx]
                        min_coord = bounding_pt2[chn_idx]
                    else:
                        max_coord = bounding_pt2[chn_idx]
                        min_coord = bounding_pt1[chn_idx]
                    # print("min_coord: ", min_coord)
                    # print("max_coord: ", max_coord)

                    for num in range(min_coord, max_coord + 1):
                        # print("num: ", num)
                        new_point = bounding_pt1
                        new_point[chn_idx] = num
                        new_point = tuple(new_point)
                        # print(new_point)
                        # line_pts.add(new_point)
                        yield new_point
        # return(line_pts)


    def intersect(self, region_obj): 
        for pt in region_obj.get_bounding_points():
            if self._in_region(pt):
                return True
        for pt in region_obj.get_bounding_lines():
            if self._in_region(pt):
                return True
        for pt in self.get_bounding_points():
            if region_obj._in_region(pt):
                return True
        for pt in self.get_bounding_lines():
            if region_obj._in_region(pt):
                return True
        return False

    def combine_region(self, region_obj):
        reg1 = self.min_max()
        reg2 = region_obj.min_max()

        if(reg1 < reg2): 
            min_point = reg1
            max_point = reg2
        else:
            max_point = reg1
            min_point = reg2
        return (min_point, max_point)



