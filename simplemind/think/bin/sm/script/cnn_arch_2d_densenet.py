"""Two-dimensional CNN architectures: DenseNet for Keras.

Method
------
DenseNet
    Builds a custom DenseNet-like architecture.
DenseNet121(input_shape, classes, activation):
    DenseNet with 121 layers
DenseNet169(input_shape, classes, activation):
    DenseNet with 169 layers
DenseNet201(input_shape, classes, activation):
    DenseNet with 201 layers
"""

import segmentation_models as sm


def DenseNet121(input_shape=None, classes=1, activation='softmax'):
    """DenseNet with 121 layers
    """
    model = sm.Unet('densenet121',input_shape=input_shape, encoder_weights=None)
    return model


def DenseNet169(input_shape=None, classes=1, activation='softmax'):
    """DenseNet with 169 layers
    """
    model = sm.Unet('densenet169',input_shape=input_shape, encoder_weights=None)
    return model


def DenseNet201(input_shape=None, classes=1, activation='softmax'):
    """DenseNet with 201 layers
    """
    model = sm.Unet('densenet201',input_shape=input_shape, encoder_weights=None)
    return model
