"""Two-dimensional CNN architectures: SEResNext for Keras.

Method
------
SEResNext
    Builds a custom SEResNext-like architecture.
SEResNext50(input_shape, classes, activation):
    SEResNext with 50 layers
SEResNext101(input_shape, classes, activation):
    SEResNext with 101 layers
"""

import segmentation_models as sm


def SEResNext50(input_shape=None, classes=1, activation='softmax'):
    """SEResNext with 50 layers
    """
    model = sm.Unet('seresnext50',input_shape=input_shape, encoder_weights=None)
    return model


def SEResNext101(input_shape=None, classes=1, activation='softmax'):
    """SEResNext with 101 layers
    """
    model = sm.Unet('seresnext101',input_shape=input_shape, encoder_weights=None)
    return model